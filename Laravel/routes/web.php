<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function () {
//     return view('welcome');
// });


Auth::routes();

Route::get('/', 'ExamenController@inicio');

Route::get('/home', 'HomeController@index')->name('home');

Route::group( ['middleware' => 'auth' ], function()
{
    Route::get('/create','ExamenController@create')->name('creacion examen');
    Route::post('/create/done','ExamenController@creater');

    Route::get('/edit/{id}','ExamenController@edit');
    Route::post('/edit/done/{id}','ExamenController@editdone');

    Route::get('/do/{id}','ExamenController@do');
    Route::post('/do/done/{id}','ExamenController@did');
    
    Route::get('/rank','ExamenController@rank');

    Route::get('/see/{id}','ExamenController@see');
});

//no se que es esto xdd
Route::get('/show/{id}','Homecontroller@show')->name('xd');