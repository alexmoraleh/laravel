@extends('layouts.app')

@section('content')
<div class="container">
                <h2>Haciendo el examen</h2>
<!-- Varajar la opcion de hacer un numero de preguntas y respuestas fijo -->
<form method="POST" action="/do/done/{{$preg[0]->id_examen}}">
<input type="text" name="nombre" value="{{$nombre->nombre}}" class="form-control-plaintext" style="font-size:30px" disabled>
    <div id="supercontenedor">
        <!-- <div class="form-group">
            <h5>Pregunta 1</h5> 
            <input type="text" name="preg[]" placeholder="Inserte aquí su pregunta" class="form-control">
        </div>
        <div class="form-group" id="contenedorpreg">
            <input type="text" name="resp[]" placeholder="Inserte aquí la respuesta" class="form-control">            
        </div> -->
        @foreach($preg as $p)
            <div class="form-group">
                <h5>{{$p->enunciado}}</h5>
                <input type="text" name="resp[]" placeholder="Inserte aquí la respuesta" class="form-control">
            </div>
        @endforeach

    </div>
    <button type="submit" class="btn btn-primary" formmethod="POST">Acabar examen</button>
 {{ csrf_field() }} 
</form>
@endsection
