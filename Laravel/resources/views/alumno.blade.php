@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Mis examenes</div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                            <th>Nombre</th>
                            <th>Fecha creación</th>
                            <th>Última modificación</th>
                            <th>Modelo</th>
                            <th>Intento</th>
                            <th>Puntuación</th>
                            <th>Reintentar</th>
                        </thead>
                        <tbody>
                            @forelse($ex as $e)
                            <tr>
                                <td>{{$e->nombre}}</td>
                                <td>{{$e->created_at}}</td>
                                <td>{{$e->updated_at}}</td>
                                <td>{{$e->id}}</td>
                                <td>{{$e->intento}}</td>
                                <td>
                                    @php
                                        $s=$e->score;
                                        $t=$e->tscore;
                                        $string=$s."/100";
                                        echo $string;
                                    @endphp
                                </td>
                                <td>
                                    <a class="btn btn-warning" href="/do/{{$e->id}}">Reintentar</a>
                                </td>
                            </tr>
                            @empty
                                <tr>
                                <td colspan="7">Sin datos</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            </br>
            <div class="card">
                <div class="card-header">Examenes disponibles</div>
                <div class="card-body">
                    <table class="table">
                    <thead class="thead-dark">
                        <tr>
                        <th>Nombre</th>
                        <th>Modelo</th>
                        <th>Última modificación</th>
                        <th>Intentar</th>
                    </thead>
                    <tbody>
                    @forelse($falta as $f)
                    <tr>
                        <td>{{$f->nombre}}</td>
                        <td>{{$f->id_examen}}</td>
                        <td>{{$f->updated_at}}</td>
                        <td>
                        <a class="btn btn-success" href="/do/{{$f->id}}" style="color:white">Hacer examen</a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="7">Sin datos</td>
                    </tr>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
    {{ csrf_field() }}
</div>
@endsection
