@extends('layouts.app')

@section('content')
<div class="container">
                <h2>Crear nuevo Examen</h2>
<!-- Varajar la opcion de hacer un numero de preguntas y respuestas fijo -->
<form method="POST" action="/create/done">
    <div id="supercontenedor">
    <div class="form-group">
        <h5>Pregunta 1</h5> 
        <input type="text" id="preg1" name="preg[1]" placeholder="Inserte aquí su pregunta" class="form-control">
    </div>
        <div class="form-group" id="contenedorpreg1">
            <input type="text" name="1resp1" placeholder="Inserte aquí su posible respuesta" class="form-control">
            <input type="radio" id="rdresp1" name="resp[1]" value="resp1" required><label for="rdresp1">Marcar como respuesta correcta</label>
        </div>
        <a id="addanswer" class="btn btn-primary" onclick="anadirres(1)" style="color:white;margin-bottom:20px">Añadir respuesta</a>
    </div>
    <div class="form-group">
        <a id="addquestion" class="btn btn-primary" onclick="anadir()" style="color:white">Añadir otra pregunta</a>
        
    </div>
    <button type="submit" class="btn btn-primary" formmethod="POST" onclick="contadores()">Crear Examen</button>
    <!-- <a onclick="contadores()">Crear Examen</a> -->
 {{ csrf_field() }} 
</form>
<script>
    var cont=2;
    var cont2=2;
    function anadirres(x){
        var txt1 = "<input type=\"text\" id=\""+x+"resp"+cont+"\" name=\"resp"+cont+"\" placeholder=\"Inserte aquí su posible respuesta\" class=\"form-control\">"
        txt1=txt1+"<input type=\"radio\" id=\"rdresp"+cont+"\" name=\"resp["+x+"]\" value=\"resp"+cont+"\"><label for=\"rdresp"+cont+"\">Marcar como respuesta correcta</label>";
        $("#contenedorpreg"+x).append(txt1);
        cont=cont+1;
    }
    function anadir(){
        var txt = "<div class=\"form-group\"><h5>Pregunta "+cont2+"</h5><input type=\"text\" id=\"preg"+cont2+"\" name=\"preg["+cont2+"]\" placeholder=\"Inserte aquí su pregunta\" class=\"form-control\"></div>"
        txt=txt+"<div class=\"form-group\" id=\"contenedorpreg"+cont2+"\"><input type=\"text\" name=\""+cont2+"resp"+cont+"\" placeholder=\"Inserte aquí su posible respuesta\" class=\"form-control\">"
        txt=txt+"<input type=\"radio\" id=\"rdresp"+cont+"\" name=\"resp["+cont2+"]\" value=\"resp"+cont+"\" required><label for=\"rdresp"+cont+"\">Marcar como respuesta correcta</label>"
        txt=txt+"</div><a id=\"addanswer\" class=\"btn btn-primary\" onclick=\"anadirres("+cont2+")\" style=\"color:white;margin-bottom:20px\">Añadir respuesta</a>"
        $("#supercontenedor").append(txt);
        cont2=cont2+1;
        cont=cont+1;
    }
    function contadores(){
        var contpregs = document.getElementById("supercontenedor").childElementCount;
        var contresp;
        var i=0;
        var aux= document.getElementById("supercontenedor").children;
        contresp = (aux[1].childElementCount)/3;
        var auxchildpreg=aux[0].children;
        var aux2=auxchildpreg[1].value
        document.getElementById("preg1").value=contresp+aux2;
        console.log(document.getElementById("preg"+1));//Esto funciona, me cago en diosito nuestro
        var cont=3;
        for (i = 2; i <= contpregs; i++) { 
            contresp = (aux[cont+1].childElementCount)/3;
            auxchildpreg=aux[cont].children;
            aux2=auxchildpreg[1].value
            document.getElementById("preg"+i).value=contresp+aux2;
            cont=cont+2;
        }
        // Version antiguo (no va xd)
        // contresp = document.getElementById("contenedorpreg"+i).childElementCount;
        // var aux=document.getElementById("preg"+i).value;
        // var aux2=$(aux+":nth-child(2)").value

        //Pasar contadores
        var txt = "<input type=\"text\" name=\"cont1\" value=\""+cont+"\" style=\"visible:none\">";
        txt=txt+"<input type=\"text\" name=\"cont2\" value=\""+cont2+"\" style=\"visible:none\">";
        $("#supercontenedor").append(txt);
        
    }
 </script> <!--Acabar el post-->



@endsection
