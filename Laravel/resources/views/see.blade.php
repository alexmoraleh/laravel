@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{$arr[0]->nombre}}</div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                            <th>Enunciado</th>
                            <th>Respuesta</th>
                            <th>Modelo</th>
                            <th>Puntuación</th>
                        </thead>
                        <tbody>
                            @php
                                $total=0;
                            @endphp
                            @forelse($arr as $e)
                            <tr>
                                <td>{{$e->enunciado}}</td>
                                <td>{{$e->respuestas}}</td>
                                <td>{{$e->id_examen}}</td>
                                <td>{{$e->score}}</td>
                            </tr>
                            @php
                                $total=$total+$e->score;
                            @endphp
                            @empty
                                <tr>
                                <td colspan="4">Sin datos</td>
                                </tr>
                            @endforelse
                            <tr>
                                <td></td><td></td><td></td>
                                <td>Total {{$total}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <a class="btn btn-warning" href="/edit/{{$arr[0]->id_examen}}">Editar</a>
            </div>
            <a class="btn btn-danger" href="/" style="color:white">Volver</a>
        </div>
    </div>
    {{ csrf_field() }}
</div>
@endsection
