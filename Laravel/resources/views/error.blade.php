<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/error.css">
    <title>Factory IDLE</title>
    <link rel="shortcut icon" href="sources/elpajaro.png" />
    <meta http-equiv="refresh" content="5;url=maincont.php">
</head>

<body onload="startTime()">
    <div id="error">
        <div id="box"></div>
        <h3>ERROR 404</h3>
        <?php
            include('config.php');
            if(isset($_SESSION['LOGGED'])){
                echo "<p>Parece que la pagina donde has entrado esta un poco <span>inestable</span> </p>
                        <p>Te sugiero que vuelvas mas tarde :D</p>
                        <p>Volver a la <a href=\"maincont.php\">pagina principal</a></p>";
            }
            else{
                echo "<p>Parece que has <span>intentado</span> entrar sin logearte</p>
                        <p>Ve a <a href=\"maincont.php\">login</a></p>";
            }
        ?>
    </div>
</body>

</html>
