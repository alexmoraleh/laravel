@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Examenes</div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                            <th>Nombre</th>
                            <th>Modelo</th>
                            <th>Última edición</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            @forelse($arr as $e)
                            <tr>
                                <td>{{$e->nombre}}</td>
                                <td>{{$e->id_examen}}</td>
                                <td>{{$e->updated_at}}</td>
                                <td><a class="btn btn-success" href="/see/{{$e->id}}">Ver</a>  <a class="btn btn-warning" href="/edit/{{$e->id}}">Editar</a></td>
                            </tr>
                            @empty
                                <tr>
                                <td colspan="4">Sin datos</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <a class="btn btn-success" href="/create" style="color:white">Crear Examen</a>
            </div>
        </div>
    </div>
    {{ csrf_field() }}
</div>
@endsection
