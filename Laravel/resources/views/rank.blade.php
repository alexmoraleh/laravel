@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Rank</div>

                <div class="card-body">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                            <th>Alumno</th>
                            <th>Examen</th>
                            <th>Fecha del intento</th>
                            <th>Modelo</th>
                            <th>Puntuacion</th>
                        </thead>
                        <tbody>
                            @forelse($arr as $e)
                            <tr>
                                <td>{{$e->name}}</td>
                                <td>{{$e->nombre}}</td>
                                <td>{{$e->updated_at}}</td>
                                <td>{{$e->id_examen}}</td>
                                <td>
                                    @php
                                        $s=$e->score;
                                        $t=$e->tscore;
                                        $string=$s."/100";
                                        echo $string;
                                    @endphp
                                </td>
                            </tr>
                            @empty
                                <tr>
                                <td colspan="7">Sin datos</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                    
                </div>
                <a class="btn btn-danger" href="/">Volver</a>
            </div>
        </div>
    </div>
    {{ csrf_field() }}
</div>
@endsection
