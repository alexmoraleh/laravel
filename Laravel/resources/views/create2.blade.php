@extends('layouts.app')

@section('content')
<div class="container">
                <h2>Crear nuevo Examen</h2>
<!-- Varajar la opcion de hacer un numero de preguntas y respuestas fijo -->
<form method="POST" action="/create/done">
    <input type="text" name="nombre" placeholder="Inserte aquí el nombre del examen" class="form-control-plaintext" style="font-size:30px" required>
    <br>
    <div id="supercontenedor">
        <div class="form-group">
            <h5>Pregunta 1</h5> 
            <input type="text" name="preg[]" placeholder="Inserte aquí su pregunta" class="form-control" required>
        </div>
        <div class="form-group" id="contenedorpreg">
            <input type="text" name="resp[]" placeholder="Inserte aquí la respuesta" class="form-control" required>            
        </div>
    </div>
    <div class="form-group">
        <a id="addquestion" class="btn btn-primary" onclick="anadir()" style="color:white">Añadir otra pregunta</a>
        
    </div>
    <button type="submit" class="btn btn-primary" formmethod="POST">Crear Examen</button>
 {{ csrf_field() }} 
</form>
<script>
var cont=2;
function anadir(){
        var txt = "<div class=\"form-group\"><h5>Pregunta "+cont+"</h5><input type=\"text\" name=\"preg[]\" placeholder=\"Inserte aquí su pregunta\" class=\"form-control\" required></div>"
        txt=txt+"<div class=\"form-group\" id=\"contenedorpreg\"><input type=\"text\" name=\"resp[]\" placeholder=\"Inserte aquí la respuesta\" class=\"form-control\" required></div>"
        $("#supercontenedor").append(txt);
        cont=cont+1;
    }
</script>
@endsection
