@extends('layouts.app')

@section('content')
<div class="container">
                <h2>Editar examen</h2>
<!-- Varajar la opcion de hacer un numero de preguntas y respuestas fijo -->
<form method="POST" action="/edit/done/{{$nombre->id_examen}}">
    <input type="text" name="nombre" value="{{$nombre->nombre}}" class="form-control-plaintext" style="font-size:30px" disabled>
    <br>
    <div id="supercontenedor">
        @php
            $cont=1
        @endphp
        @foreach($pregs as $preg)
        <div class="form-group">
            <h5>
            Pregunta 
            @php
                echo $cont;
                $cont=$cont+1;
            @endphp
            </h5> 
            <input type="text" name="preg[]" value="{{$preg->enunciado}}" class="form-control" required>
        </div>
        <div class="form-group" id="contenedorpreg">
            <input type="text" name="resp[]" value="{{$preg->respuestas}}" class="form-control" required>            
        </div>
        @endforeach
    </div>
    <button type="submit" class="btn btn-primary" formmethod="POST">Modificar Examen</button>
 {{ csrf_field() }} 
</form>
@endsection
