<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Examen;
use App\Nombreexamen;
use App\Pregunta;
use App\Intento;
use Auth;
use App\User;

class ExamenController extends Controller
{
    public function inicio(){
        if($user=Auth::user()){
            if($user->profesor==0){
                $ex=DB::table('examens')->where('id_user','=',$user->id)->get();
                $faltan=DB::table('examens')->select('id')->where('id_user','=',$user->id)->get();
                // $falta=Nombreexamen::All()->where('id_examen','<>',$ex[0]->id);
                $falta=DB::select('SELECT * FROM nombreexamens WHERE id_examen NOT IN (SELECT id FROM examens where id_user=?)',[$user->id]);
                return view('alumno', compact('ex','falta'));
            }
            else{
                $arr=Nombreexamen::All();
                return view('profe', compact('arr'));
            }
        }
        else{
            return view('welcome');
        }
    }   
    public function rank(){
        $arr=DB::select('SELECT name, nombre, examens.updated_at, examens.id as id_examen, score, tscore FROM examens JOIN users ON examens.id_user=users.id GROUP BY examens.id ORDER BY examens.id, score');
        return view('rank', compact('arr'));
    }
    public function see($id){
        if($user=Auth::user()){
            if($user->profesor==1){
                $arr=DB::select('SELECT enunciado, respuestas, preguntas.id_examen as id_examen, score, nombre FROM preguntas JOIN nombreexamens ON nombreexamens.id_examen=preguntas.id_examen WHERE preguntas.id_examen=?',[$id]);
                return view('see',compact('arr'));
            }
            else{
                echo "<h2>No eres profe</h2>";
                return abort(404);
            }
        }
        else{
            return view('welcome');
        }
    }
    public function create(){
        //Cont2=preguntas, cont1=respuestas
        // $ex = new Examen();
        // $cont=1;
        // $aux=0;
        // $numresp=1;
        // $preguntas=array($request['preg']);
        // // for($pregcont=1;$pregcont<$request->cont2;$pregcont++){
        // //     $preg = new Pregunta();
        // //     $preg->enunciado=$request->preg+$cont2;//esto va??
        // //     for($respcont=1;$respcont<$request->cont1;$respcont++){
                
        // //     }
        // // }
        // $contresp=0;
        // for($cont=1;$cont<$request->cont2;$cont++){ //el for no esta bien hecho
        //     $preg = new Pregunta();
        //     $contresp=substr($request->preg[$cont],0,1);
        //     for($aux=1;$aux<($contresp+1);$aux++){
        //         //acabar de mirar lo de resp[1] y a ver si ya funciona lo de meter el num delante de la pregunta
        //     }
        // }
        // echo $contresp;
        // print_r($preguntas);
        // echo array_get($preguntas);
        // $request->
        // return view('prueba');
        // lo de arriba es el intento del primer examen, vamos a lo facil xd
        return view('create2');
    }
    public function creater(Request $request){
        $user= new User();
        $user=Auth::user();
        $prof=$user->profesor;
        
        if($prof==1){//esto lo haré luego
            $pregunta = new Pregunta();
            $idexamen=Pregunta::max('id_examen');//sacar aqui el MAX(id) de examen
            if(empty($idexamen)){
                $idexamen=0;
            }
            $idexamen=$idexamen+1;
            $cont=0;
            $preguntaarr=$request->preg;
            $resp=$request->resp;
            foreach($preguntaarr as $preg){
                $pregunta = new Pregunta();
                $pregunta->enunciado=$preg;
                $pregunta->respuestas=$resp[$cont];
                $pregunta->score=rand(10,50);
                $pregunta->tipo="ESCRITA";
                $pregunta->correcta=1;
                $pregunta->id_examen=$idexamen;
                // echo $pregunta->enunciado;
                // echo $pregunta->respuestas;
                $pregunta->save();
                $cont=$cont+1;
                // print_r($resp);
            }
            $nombre=new Nombreexamen();
            $nombre->nombre=$request->nombre;
            $nombre->id_examen=$idexamen;
            $nombre->save();
            return redirect('/');
        }
        else{
            echo "<h2>No eres profe</h2>";
            return abort(404);
        }
    }
    public function edit($id){
        $pregs=Pregunta::where('id_examen','=',$id)->get();
        $nombre=Nombreexamen::where('id_examen','=',$id)->first();
        return view('edit', compact('pregs','nombre'));
    }
    public function editdone(Request $request, $id){
        $user= new User();
        $user=Auth::user();
        $prof=$user->profesor;
        
        if($prof==1){
            $cont=0;
            $preguntaarr=$request->preg;
            $resp=$request->resp;
            $preguntas=Pregunta::where('id_examen','=',$id)->get();
            foreach($preguntaarr as $preg){
                $pregunta = $preguntas[$cont];
                $pregunta->enunciado=$preg;
                $pregunta->respuestas=$resp[$cont];
                $pregunta->save();
                $cont=$cont+1;
            }
            $corrpreg=DB::table('preguntas')->where('id_examen','=',$id)->get();
            $corresp=DB::table('intentos')->where('id_examen','=',$id)->get();
            $cont=0;
            $score=0;
            $tscore=0;
            $aux=0;
            foreach($corrpreg as $preg){
                if($preg->respuestas==$corresp[$cont]->respuesta){
                    $score=$score+$preg->score;
                }
                $tscore=$tscore+$preg->score;
                $cont=$cont+1;
            }
            $score=($score*100)/$tscore;
            $examen=DB::table('examens')->where('id','=',$id)->first();
            $scoreexam=$examen->score;
            $scoreexam=($scoreexam*100)/$tscore;          
            DB::update('UPDATE examens SET score=? where id=?',[$score,$id]);
            return redirect('/');
        }
        else{
            echo "<h2>No eres profe</h2>";
            return abort(404);
        }
    }
    public function do($id){
        $preg = DB::table('preguntas')->where('id_examen','=',$id)->get();//array de preguntasç
        $ex=DB::table('examens')->where([['id','=',$id],['id_user','=',Auth::user()->id]])->first();
        // $intent=head($ex);
        if(empty($ex)){
            $ex=new Examen();
            $ex->intento=0;
        }
        if($ex->intento<3){
            $nombre=DB::table('nombreexamens')->where('id_examen','=',$id)->first();
            return view('do', compact('nombre','preg'));
        }
        else{
            echo "<h2>No te quedan intentos</h2>";
            return abort(404);
        }
    }
    public function did($id,Request $request){
        $max=Intento::where('id_examen','=',$id)->max('intento');
        $max=$max+1;
        $cont=0;
        $respuesta=$request->resp;
        $int=new Intento();
        foreach($respuesta as $r){
           $resp=new Intento();
           $resp->id_examen=$id;
           $resp->respuesta=$r;
           $resp->intento=$max;
           $resp->id_user=Auth::user()->id;
           $resp->save();
           $cont=$cont+1;
        }
        $corrpreg=DB::table('preguntas')->where('id_examen','=',$id)->get();
        $corresp=DB::table('intentos')->where([['id_examen','=',$id],['intento','=',$max]])->get();
        $cont=0;
        $score=0;
        $tscore=0;
        $aux=0;
        foreach($corrpreg as $preg){
            if($preg->respuestas==$corresp[$cont]->respuesta){
                $score=$score+$preg->score;
            }
            $tscore=$tscore+$preg->score;
            $cont=$cont+1;
        }
        $score=($score*100)/$tscore;
        if ($max>1){
            $examen=DB::table('examens')->where('id','=',$id)->first();
            // $totalex=head($examen);
            $scoreexam=$examen->score;
            $scoreexam=($scoreexam*100)/$tscore;
            if($scoreexam<$score){
            // if($examen->score<$score){
            //     $examen->intento=$max;
            //     $examen->score=$score;
            //     $examen->save();
            
                DB::update('UPDATE examens SET intento=?, score=? where id=?',[$max,$score,$id]);
                 //$ex=$totalex[0];
                // $ex->intento=$max;
                // $ex->score=$score;
                // $ex->save();
            }
            else{
                DB::update('UPDATE examens SET intento=? where id=?',[$max,$id]);
                // $ex=$examen[0];
                // $ex->id=$id;
                // $ex->intento=$max;
                // $ex->save();
            }
        }
        else{
            $nombre=DB::table('nombreexamens')->where('id_examen','=',$id)->first();
            $ex=new Examen();
            $ex->id=$id;
            $ex->id_user=Auth::user()->id;
            $ex->nombre=$nombre->nombre;
            $ex->modelo="A";//Cuando cree la interficie esto se cambia (spoiler: no)
            $ex->intento=$max;
            $ex->score=$score;
            $ex->tscore=$tscore;
            $ex->save();
        }
        return redirect('/');
    }
}
