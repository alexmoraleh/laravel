<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreguntasTable extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preguntas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('enunciado');
            $table->string('tipo');//Hay un enum en config/enums.php, no se como se usa
            $table->string('respuestas');//String de todas las respuestas encapsuladas por {},{},...
            $table->integer('correcta');//indice de la correcta
            //Si me animo usaré los campos de arriba para crear varias respuestas con radiobutons y tal, demomento haré pregunta respuesta normal y viendo el tiempo que
            //me queda se va a quedar asi :(
            // $table->integer('id_examen')->unsigned();
            // $table->foreign('id_examen')
            // ->references('id')->on('examens')
            // ->onDelete('cascade');
            $table->integer('id_examen');
            $table->integer('score');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preguntas');
    }
}
