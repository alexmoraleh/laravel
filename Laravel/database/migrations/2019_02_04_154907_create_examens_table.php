<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {//He tocado esto despues de hacer un migrate, igual la he cagado al hacer drop a la tabla :/
        Schema::create('examens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')
            ->references('id')->on('users')
            ->onDelete('cascade');
            $table->string('nombre');
            // $table->integer('id_preguntas')->unsigned();
            // $table->foreign('id_preguntas')
            // ->references('id')->on('preguntas')
            // ->onDelete('cascade');
            //Esto era para el modelo de examen to chungo...
            $table->timestamps();
            $table->string('modelo');
            $table->integer('intento');
            $table->integer('score');
            $table->integer('tscore');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('examens');
    }
}
