<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntentoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Intentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_examen');
            // $table->foreign('id_examen')
            // ->references('id_examen')->on('preguntas')
            // ->onDelete('cascade');
            $table->string('respuesta');
            $table->integer('intento');
            $table->integer('id_user')->unsigned();
            $table->foreign('id_user')
            ->references('id')->on('users')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Intentos');
    }
}
